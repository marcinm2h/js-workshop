---
theme : 'white'
# transition: "zoom"
highlightTheme: 'darkula'
---


# Co to jest AJAX?

  - Asynchchronous JavaScript And XML

---

# Pobieranie treści z serwera

  ```javascript
    // XMLHttpRequest - stare api - oparte na callbackach
    const xhr = new XMLHttpRequest();
    xhr.onloadend = function() {
      if (this.readyState == 4 && this.status == 200) { // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState
        console.log(JSON.parse(this.responseText /* responseText jest zawsze plaintextem */));
      }
      if (this.readyState == 4 && this.status == 200) {
        console.error('404');
      }
    };

    xhr.open('GET', 'https://swapi.co/api/people/1', true /* async */);
    xhr.send();
  ```

---

  ```javascript
    // fetch - nowe api - oparte na promise'ach
    fetch('https://swapi.co/api/people/1')
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          console.error(response, response.status /* 404 */);
        }
      })
      .then(json => console.log(json));
  ```

---

# Dynamiczne generowanie treści

  ```javascript
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/users', true);
    xhr.send(JSON.stringify({ name: 'Luke', gender: 'male' })); // w metodzie 'send' wysyłamy body requestu
  ```

---

# Różne formaty danych

  - plaintext
  - XML
  - JSON

---

# Format JSON

  - JavaScript Object Notation

---

#  jQuery

  ```javascript
    // https://api.jquery.com/jQuery.Ajax/
    const $getPerson1 = $.ajax('https://swapi.co/api/people/1', {
      method: 'GET',
      success: (data) => {
        console.log(data); //json
      }
    });
    // To samo co 'success' - API obsługuje callbacki jak i promise'y
    $getPerson1.done(data => {
      console.log(data);
    });

    $getPerson1.always(data => {});

    // https://api.jquery.com/jQuery.get/
    $.get('https://swapi.co/api/people/1')
      .done(response => console.log(response));
  ```

---

# Axios

```javascript
  // https://github.com/axios/axios
  axios.get('https://swapi.co/api/people/1')
    .then(function(response) {
      console.log(response);
    })
    .catch(function(error) {
      console.log(error);
    })
    .then(function() {
      // .always
    });
```