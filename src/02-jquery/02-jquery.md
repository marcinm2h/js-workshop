---
theme : 'white'
# transition: "zoom"
highlightTheme: 'darkula'
---

# Czym jest jQuery

- biblioteka, o filozofii "write less, do more"
- jedno api uspójniające różnice między przeglądarkami

---

## Manipulacja DOM i poruszanie się po drzewie DOM

```javascript
      // http://api.jquery.com/category/manipulation/
      // https://api.jquery.com/category/traversing/
      // https://learn.jquery.com/using-jquery-core/selecting-elements/
      // https://learn.jquery.com/using-jquery-core/manipulating-elements/
      //.append(), .prepend(), .val(), .find(), .filter(), .first()
      $('#cart').find('.item').first();
  ```

---

  ## Obsługa eventów (zdarzeń)

    ```javascript
      // https://api.jquery.com/category/events/
      // .on(), .off(), .click(), .dblckick(), .focus(), .blur(), .trigger()
      $('p').on('click', function(e) { console.log(e.target) });
    ```

---

  ## Animacje

    ```javascript
      // https://api.jquery.com/category/effects/
      // .animate(), .fadeIn(), .fadeOut(), .hide(), .toggle(), ...
      $('p').animate({ left: '+=100' }, 500);
    ```

---

  ## Iterowanie
  ```javascript
    // Jeśli selektor $(selektor) wybierze więcej niż 1 element - 
    // metody automagicznie przeiterują się po kolekcji elementów
    // i wykonają się dla każdej z nich
    $('a') // selektor wskazuje na listę linków o długości > 1
    .fadeOut(); // wszystkie linki znikną
  ```

  ```javascript
    // Można też iterować się po kolekcji explicite
    $('div').each(function(idx) {
      if (this.style.color !== 'blue') {
        this.style.color = 'blue';
      } else {
        this.style.color = ';
      }
    });
  ```
---

  # Ajax

  ```javascript
  // https://api.jquery.com/category/ajax/
  // $.ajax(), $.get(), $.post()
  $.ajax('/users/01', {}).done(function(data) { console.log(data); })
  ```

---

# Sposób osadzania w dokumencie HTML

---

  ## `<script />`

  ```html
    <script defer src="jquery.min.js"></script>
  ```

---

  ## moduły es6

  ```html
    <script type="module" src="index.js"></script>
  ```

---

  ## [CDN](https://code.jquery.com/)
  
  ```html
    <script defer src="https://code.jquery.com/jquery-3.3.1.js"></script>
  ```

---

  ## Bundlery

    - https://github.com/babel/babelify
    - webpack

---

# Struktura kodu

  - https://learn.jquery.com/code-organization/concepts/
  - moduły es6

---

  ```javascript
    // document się załadował
    // - document.readyState === 'complete'
    // - DOM jest gotowy
    $(document).ready(function() { console.log('document.ready'); });
    $(function() { console.log('document.redady'})

    // strona się załądowała - włącznie z obrazkami i frame'ami
    $(window).on('load', function() { console.log('window.load'); })
  ```

---

# Dostęp do elementów strony

  ```javascript
    $('.selector');
  ```

---

# Co to jest DOM

  - http://bioub.github.io/d3.DOMVisualizer/

---

# Manipulowanie węzłami DOM

  - trawersowanie i manipulacja

---

# Obsługa zdarzeń

  ```javascript
    $('.selector').on('click', function(e) {
      console.log(e.target, this);
      // this jest zbindowane na element $('.selector')
      // e.target - element wywołjący event, który
      // został zbąbelkowany do '.selector'
      // może to być sam '.selector'
    });
  ```

---

# Dodawanie animacji na stronie

  ```javascript
    $('p').animate({ opacity: 0.3, left: '+=100' }, 300);
  ```

---

# Widżety jQuery UI

  - https://jqueryui.com/
