---
theme : 'white'
# transition: "zoom"
highlightTheme: 'darkula'
---

# Wprowadzenie do języka JavaScript

---

- skryptowy, dynamiczny, nietypowany język
- aktualnie jedyny język przeglądarek (w przyszłośći WASM)
- dzięki node - obecny na backendzie

---

![Good Parts](assets/good-parts.jpg "Good Parts")

---

![YDKJS](assets/ydkjs.jpg "YDKJS")

---

# JavaScript a przeglądarki

- https://caniuse.com/
- ES5 (https://caniuse.com/#feat=es5)
- ES6(ES2015) i nowsze - transpilacja do ES5

---

# Umieszczanie skryptów w dokumencie HTML
```html
  <script>console.log('script')</script>
  <!--type="text/javascript" od HTML5 jest opcjonalne -->
  <script src="./script.js"></script>
  <script src="http://example.com/script.js"></script>
```

---

 ## Pod body vs w head
 
 - Czy elementy DOM istnieją
 - https://jsfiddle.net/xqwkuyds/34/

---

 - domyślnie jest synchroniczne - i blokuje renderowanie do wczytania skryptu

---

##  async i defer

 https://www.growingwiththeweb.com/2014/02/async-vs-defer-attributes.html

---

## Moduły AMD

  ```javascript
  define(['underscore', 'jquery'], function(_, $) {
    $('html').on('click', function() {
      _.each([1, 2, 3], alert)
    });
  })
  ```

---

## Moduły CommonJS

  ```javascript
  const $ = require('jquery');

  function fn() {
    $('html').on('click', function() {
      console.log('fn');
    });
  }

  exports.fn = fn;
  ```

---

## Moduły es6

  ```javascript
  import $ from 'jquery';

  export function fn() {
    $('html').on('click', function() {
      console.log('fn');
    });
  }
  ```

---

# Konsola przeglądarki

  - elements
  - sources
  - network
  - inne

---

# Wyświetlenie okna dialogowego

 - confirm, alert, prompt

---

# Tworzenie komentarzy

 - //, `/* */`

---

# Typy danych
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Data_types
- Primitives: Boolean, Null, Undefined, Number, String, Symbol; Objects

---

# Rodzaje operatorów
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators

---

# Instrukcje warunkowe

  - if, else, else if
  - switch
  - true && fn()

---

# Tworzenie pętli

  - while, for,
  - for in - iteracja po indexach, kluczach obiektu,
  - ES6: for of - po obiektach zawierajacych iterator;
  - forEach, map, reduce;

---

# Tworzenie podstawowych funkcji

  - let, var, const, global
  - function fn() {} vs const fn = () => {}
  - () => {}
  - IIFE

---

# Zasięg zmiennych

  - var - lexical scope (zw. z funkcją - każda funkcja nowy scope)
  ```javascript
  function fn() {
    var x = 'x';
    if (true) {
      var b = 'b';
    }
    console.log(b, x);
  }
  fn() //output?
  ```

---

  - w ES6 nie używamy var!

---

  ![Make const not var](assets/make-const.jpg "Make const not var")

---

  - block scope

  ```javascript
  function fn() {
    const x = 'x';
    if (true) {
      const b = 'b';
    }
    console.log(b, x);
  }
  fn() //output?
  ```

---

# Funkcje globalne

  - encodeURI / encodeURIComponent
  - decodeURI / decodeURIComponent 
  - isFinite / isNaN
  - parseInt / parseFloat
  - Number / String / Array / Object / RegExp / Symbol
  - eval

---

# Dostępne obiekty w przeglądarce

- window
- document
- history
- location
- navigator
- ...

---

# Zdarzenia (events)
