# W skrócie

```javascript
  console.log('Wiadomość', { obiekt: 'obiekt' });

  console.info('Wersja aplikacji 1.0.2');
  console.warn('Ostrzeżenie: metoda x() jest przestarzała. Użyj metody y().'); //  tekst
  console.error('Wystąpił błąd.'); // czerwony tekst

  debugger; //pauzuje wykonanie skryptu
```

---

# Inne

```javascript
  console.dir(document.querySelector('a')); // atrybuty (properties) elementu
  const a = { name: 'user', login: 'user' };
  const parsed = JSON.stringify(a, null, 2);
  // {
  //   "name": "user",
  //   "login": "user"
  // }
  copy(a); //kopiuje do schowka
```


---

# Wszystko

```javascript
  const el = document.querySelector('a');
  console.dir(el);
  console.log(el); // w Chrome console.log elementu wyświetli referencję do niego, a console.dir jego atrybuty
  console.warn('warning');
  console.error('error');
  console.table([0, 1, 2, 3, 4]);
  console.table({ a: 'a', b: 'b' });

  // grupowanie logów
  console.group('user::info'); //lub console.groupCollapsed()
  console.log('fetching user info');
  console.log('user info fetched');
  console.groupEnd();
  // koniec grupy

  // mierzenie czasu wykonania
  console.time('id--001');
  //ciężkaLubAsynchronicznaOperacja()
  console.timeEnd('id--001');
  //koniec pomiaru czasu

  debugger; //pauzowanie skryptu

  copy('tekst')
```
