# Dziedziczenie prototypowe
prototype / class
https://tylermcginnis.com/beginners-guide-to-javascript-prototype/

# Asynchroniczość
```javascript
  console.log(0, 'sync')
  setTimeout(() => {console.log(1, 'async')}, 0);
  setTimeout(() => {console.log(2, 'async')}, 100);
  setTimeout(() => {console.log(3, 'async')}, 1000);
  setTimeout(() => {console.log(4, 'async')}, 100);
  console.log(3, 'sync');
```