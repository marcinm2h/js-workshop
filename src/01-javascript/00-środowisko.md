# Repl
- [JSFiddle](https://jsfiddle.net/) - szybki notatnik HTML/JS/CSS + podgląd
- [CodeSandbox](https://codesandbox.io) - VSCode online
- [StackBlitz](https://stackblitz.com) - To co wyżej, ale nieco lżejsze

# Środowisko
- Node / Npm
- npm install -g http-server
- Chrome
- [NVM](https://github.com/creationix/nvm) - zarządzanie wersjami node
