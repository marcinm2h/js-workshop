---
theme : 'white'
# transition: "zoom"
highlightTheme: 'darkula'
---

# escapowanie

  - encode - escapowanie (zamiana) znaków specjalnych do URI
  - decode - dekodowanie escapowanych stringów
  - przy pracy z inputem użytkownika

---

  - encode/decode URI - do stringów - URI -> nie escapuje części znaków jak np. ("http://")
  - encode/decode URI component - do stringów - części urla - np. parametrów ("userInput=input ze spacjami")

```javascript
  encodeURI("http://www.example.org/nazwa pliku ze spacjami.html");
  // http://www.example.org/nazwa%20pliku%20ze%20spacjami.html
  encodeURIComponent("nazwa pliku")
  // nazwa%20pliku
  decodeURI("http://www.example.org/nazwa%20pliku%20ze%20spacjami.html")
  // http://www.example.org/nazwa pliku ze spacjami.html
  decodeURIComponent("nazwa%20pliku")
  // nazwa pliku

```

---


# isFinite / isNaN

  - isFinite - sprawdza czy jest to liczba wymierna - jest typu number, nie jest NaN (typeof NaN === 'number'), nie jest Infinity, lub -Infinity
  - isNaN - sprawdza czy jest to NaN
  - przydatne przy pracą z parsowaniem danych z serwera, gdzie spodziewamy się liczby, ale nie mamy zaufania do api :)

---

```javascript
  isNaN(null); // false
  isNaN(0); // false
  isNaN(Infinity); // false
  isNaN(NaN); // true

  isFinite(1); // true
  isFinite(NaN); // false
  isFinite(Infinity); //false
  isFinite(-Infinity); //false

  const user =  { login: '123' };
  const userAge = parseInt(user.age); // parseInt(undefined) === NaN
  isFinite(userAge); // false
```

---

# Konstruktory

- Number
- String
- Array
- Object
- RegExp
-  Symbol

---

# eval

  - parsowanie stringa na wykonany kod js
  - rzadko używane - łatwo o lukę bezpieczeństwa

