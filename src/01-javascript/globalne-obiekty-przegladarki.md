# Window
- https://developer.mozilla.org/en-US/docs/Web/API/Window

# History

```javascript
  window.onpopstate = function(event) {
    alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
  };

  history.pushState({page: 1}, "title 1", "?page=1");
  history.pushState({page: 2}, "title 2", "?page=2");
  history.replaceState({page: 3}, "title 3", "?page=3");
  history.back(); // alerts "location: http://example.com/example.html?page=1, state: {"page":1}"
  history.back(); // alerts "location: http://example.com/example.html, state: null
  history.go(2);  // alerts "location: http://example.com/example.html?page=3, state: {"page":3}
```

# Document
- document.querySelector, document.addEventListener,
https://developer.mozilla.org/en-US/docs/Web/API/Window/document

# Location
- The Location interface represents the location (URL) of the object it is linked to.
https://developer.mozilla.org/en-US/docs/Web/API/Location

# Navigator
- The Navigator interface represents the state and the identity of the user agent
- navigator.userAgent
- api urządzenia, np. bateria: navigator.getBattery().then(b => console.log(b)), geolokacja https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API

https://developer.mozilla.org/en-US/docs/Web/API/Navigator