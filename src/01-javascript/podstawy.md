# Zmienne i komentarze

```javascript
  globalA = 'A'; //jeśli nie jest re-deklaracja - przypisuje do globalnego obkeitu
  var a = 'a';
  a  = 'b';
  /*
  Powyższe deklaracje zmiennych
  są nie polecane - "use const not var"
  */

  window.globalX = ''; // w przeglądarce globalnym obiektem jest window, window.globalA === globalA
  let b = 'b';
  const c = 'c'; // constów nie można re-deklarować
  c = 'd' // zwróci błąd
  const d = 'd', e = 'e', f ='f' //deklaracja w 1 linii
  //średniki są opcjonalne, ale automatycznie wstawiane przez silnik
```

# Typy danych

## Primitives - typy podstawowe / prymitywne
```javascript
  const isAsync = true; //Boolean 
  typeof isAsync === 'boolean';

  let user = null; //Null
  typeof null === 'object'// true

  let newUser = undefined;
  let user2; // Undefined
  newUser === undefined// true
  typeof newUser //'undefined'

  let userId = 100, version = 1.2; // Number
  0.1 + 0.2; //0.30000000000000004
  1*2;
  2**2;
  typeof 1; /// 'number'
  typeof NaN; // 'number'

  let x = 'username', y = "password", z = `Id użytkownika to ${userID}`; //String
  x.length; //8 - mimo, że string to primitive - posiada otczkę w postaci String
  let id01 = Symbol('ido1'); //Symbol - niezmienne - jako klucze w obiektach
```

---

## Typy obiektowe - Object
```javascript
  const o = {}; // literał
  const obj = { a: 'a', b: { userName: 'm' } };
  obj.a = 'x'; // const'y są nie re-deklarowalne, ale obiekty nadal są zmienne
  obj['b'];

  const name = 'user01';
  const id = '001';
  const url = '/users/001';
  const user01 = { id, name, url };

  const a = { a: 'a' };
  const b = { a: 'a' };
  a === b; // false - porównuje referencje
  a == b; // false - porównuje referencje
  //https://lodash.com/docs/4.17.11#isEqual
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
```

# Typy obiektowe - tablice 

```javascript
  const array = ['tablica', 01, { o: 'o'}, user01];
  array[0] // 'tablica'
  Array.isArray(array) //true - metoda es6

  const array1 = [0, 1, 2];
  const array2 = [...array, 3, 4]; //0,1,2,3,4
  const array1_copy = [0, 1, 2];
  array1 === array1_copy // false - poróœnuje referenjce
  //https://lodash.com/docs/4.17.11#isEqual
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

```

---

# Typy obiektowe - regexp

```javascript
  // https://regex101.com/
  /[0-9]/ //literał
  const re = new RegExp('[0-9]'); //konstruktor - przydatny przy dynamicznie generowanych regexach
  re.test('1'); // true
  'asd123'.match(re); // ["1", index: 3, input: "asd123", groups: undefined]
  'asd1a2'.replace(/[0-9]/g, '_') // asd_a_ - flaga 'g' - global
```

---

# Operatory - matematyczne

```javascript
  // +, -, *, /, **, %,
  1+1; 1-1; 2*2; 4/2; 2/1; 10 % 5 /* modulus - reszta z dzielenia*/;
  // ++, --
  let a = 1;
  a++; // a = a+1; a += 1;
  a === 2; // true
```

# Operatory - przypisanie

```javascript
  // =, +=, -=, *=, /=, %=
  let a = 1;
  a +=1; // a = a + 1;  2
  let b = 2;
  b -= 1 // b = b-1; 1
```

---

# Operatory - porównanie
```javascript
  // ===, !==, ==, !=,  >, <, >=, <=
  let boolA = true;
  let numB = 1;
  // dlaczego NIE używamy porównanie z rzutowaniem - ==
  boolA == numB; // true
  0 == false; // true
  '' == false; // true
  // używamy TYLKO ===
  boolA === numB; // false
```

---

# Operatory - logiczne

```javascript
  // &&, ||, !, ^, ?:
  if (true && true) { console.log('wykona się') }
  if (true && false) { console.log('nie wykona się') }

  if (true || true) { console.log('wykona się') 
  if (true || false) { console.log('wykona się') }


  if (!false) { console.log('wykona się') }
  if (!true) { console.log('nie wykona się') }

  const a = {
    b: {
      c: {
        d: 'username';
      }
    }
  };
  const username = a && b && c && d;
  const terenary = true ? 'a' : 'b'; 

```

---
 
# Operatory - bitowe

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators

---

# Instrukcje warunkowe

```javascript
  const user = { login: '' };
  if (!user.login) {
    service.setLogin(user, 'login01');
  } else {
    console.log('Użytkownik ma ustawiony login');
  }

  const lang = 'pl';
  switch (lang) {
      case 'pl':
          console.log('Cześć!');
          break;
      case 'en':
          console.log('Hi!');
          break;
      case 'es':
          console.log('Hola!');
          break;
      default:
          console.log('Hello World!');
  }
```

---

