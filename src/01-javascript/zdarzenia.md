# Eventy - zdarzenia

 - Wszystkie elementy HTML (tak na prawdę węzły DOM) generują eventy (zdarzenia)
 - Możemy tworzyć własne eventy (architektura oparta na eventach/ wzorzec Pub/Sub)

---

# Niektóre przydatne eventy

## Mysz

  - click - kliknięcie na element
  - contextmenu - kliknięcie prawym przyciskiem myszy
  - mouseover / mouseout - najechanie myszką na element
  - ...

```javascript
  button.addEventListener('click', function(e) {
    input.focus(); // po wciśnięciu przycisku przejdź do wypełnienia formularza
  });

  document.addEventListener('mousemove', function(e) {
    console.log(e.clientX, e.clientY); // logowanie pozycji kursora
  });
```

## Klawiatura

  - keypress - wciśnięcie przycisku na klawiaturze i puszczenie go 
  - keydown - wciśnięcie przycisku na klawiaturze 
  - keyup - puszczenie przycisku na klawiaturze 

```javascript
  document.addEventListener('keydown', function(e) {
    if (e.which === 37 /* wciśnięcie strzałki w lewo */) {
      slider.prev();
    }
    if (e.which === 39 /* wciśnięcie strzałki w prawo */) {
      slider.next();
    }
  });
```

## Formularz

  - submit - użytkownik wysyła formularz
  - focus / blur - wejśćie / wyjście z pola

```javascript
  form.onsubmit = function(e) {
    e.preventDefault(); // wstrzymanie domyślnej akcji przegladarki
                        // - w tym przypadku przy formularzu
                        // gdzie wysyłąmy dane do serwera
                        // - submit wiązałby się z wyjściem ze strony
                        // - czemu chcemy zapobiec
    validateForm(e.target); // targetem - celem eventu
                            // jest element html - w tym przypadku formularz
  }
```

## Inne

  - load - element załadował się - np. przy ładowaniu obrazów - obrazek ściągnął się i możemy poprawnie policzyć jego wysokość
  - scroll - w trakcie scrollowania w obrębie elementu
  - DOMContentLoaded - elementy html się załądowały (uwaga! załadowany element <img /> nie jest równoznaczny z pobranym obrazkiem - jedynie utworzony jest obszar w którym się on wyrenderuje)

```javascript
  // ładujemy obrazek mimo nie pokazania go na stronie;
  // można w ten sposób przyspieszyć renderowanie się długiej strony -
  // kiedy obrazek jest w polu widzenia przeglądarki (viewport) - pokazując go
  // i chowając kiedy wychodzi poza ekran
  const img = new Image();
  img.onload = function(e) {
    console.log(this.width, this.height);
  }
  img.src = 'https://www.google.pl/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png';
```

---

# Obsługa zdarzeń (event handlers)

  - jako atrybut html

```html
  <script>
    function action(text) {
      alert(text);
    }
  </script>
  <button id="btn" onclick="alert('uwaga');action(this.innerText)">Uwaga!</button>
```

  - property elementu ustawiony w javascript

```javascript
  window.btn.onclick = function(e) {
    console.log('Uwaga!'); // nadpisuje atrybut html
  }
```

  - event listener

```javascript
  function onClickBtn() {
    console.log('btn');
  }
  btn.addEventListener('click' , onClickBtn);
  btn.removeEventListener( "click", onClickBtn);
```

---

# Niektóre domyślne akcje

  - event.preventDefault() zatrzymuje domyślną akcję eventu

```javascript
  window.menu.addEventListener('contextmenu', function(e) {
    e.preventDefault(); //zapobiega otwarciu menu kontekstowego
    handleMenu();
  });
```

```html
  <a href="#" onclick="event.preventDefault()">+</a>
  <!-- zapobiega zmianie urla w przeglądarce i przeładowaniu strony -->
```

```javascript
  form.onsubmit = function(e) {
    e.preventDefault(); // zapobiega przekierowaniu na url formularza
  }
```

---

# Event bubbling

- Event wywołany na elemencie-childrenie zostaje przekazany do góry - 
  do jego parenta, aż do najwyższego (html)
  <img src="https://javascript.info/article/bubbling-and-capturing/event-order-bubbling.png" />

```html
  <div id="parent" onclick="alert('1')">1
    <div onclick="alert('2')">2
      <button id="btn" onclick="alert('3')">3</button>
    </div>
  </div>
  <!-- Kliknięcie w przycisk spowoduje wywołanie wszystkich 3 alertów
  w kolejności 3, 2 , 1  -->
```

- event.stopPropagation() zatrzymuje bubbling eventu w górę

```javascript
  window.parent.onclick = function(e) {
    alert('parent');
  }

  window.btn.onclick = function(e) {
    e.stopPropagation(); // alert('parent') się nie wyświetli
    alert('btn');
  }
```

- event.target wskazuje na element na którym wywołano event,
 a event.currentTarget (jak i 'this') wskazuje na element
 na którym ustawiono event handler

```javascript
  window.parent.addEventListener('click', function(e) {
    console.log(e.target, e.currentTarget, this); // button, #parent, #parent 
  });
```

# Event capturing

- rzadko używane
- jest to etap przed bubblingiem - event jest propagowany z góry do dołu
- http://next.plnkr.co/edit/?p=preview&utm_source=legacy&utm_medium=worker&utm_campaign=next&preview
<img src="https://javascript.info/article/bubbling-and-capturing/eventflow.png" />

---

# Delegacja zdarzeń (event delegation)

- handler do eventu ustawiany jest na element - parent,
  przez co możemy obsłużyć wiele childrenów jednocześnie

```html
  <div id="menu">
    <button data-action="zoomIn">Powiększ</button>
    <button data-action="zoomOut">Pomniejsz</button>
    <button data-action="exit">Wyjdź</button>
  </div>
  <script>
    window.menu.addEventListener('click', function(e) {
      const actionHandlers = {
        zoomIn: () => alert('zoomIn'),
        zoomOut: () => alert('zoomOut'),
        exit: () => alert('exit')
      };
      let action = event.target.dataset.action;
      if (action) {
        actionHandlers[action]();
      }
    });
  </script>
```
