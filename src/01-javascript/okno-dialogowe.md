# Okna dialogowe

```javascript
  // Wszystkie okna dialogowe są sychroniczne
  // - blokują wykonanie kodu i renderowanie strony.
  alert('Wiadomość!'); // [ OK ]
  const isAdult = confirm('Czy masz 18 lat?'); // [ Annuluj | OK ]
  console.log(isAdult); // true || false
  const name = prompt('Podaj swoje imię');
  prompt('Wpisz swój wiek', '20'); // prompt domyślnie wypełniony '20'

```