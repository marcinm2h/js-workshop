# JavaScript

- wprowadzenie do języka JavaScript
- JavaScript a przeglądarki
- umieszczanie skryptów w dokumencie HTML
- konsola przeglądarki
- wyświetlanie okna dialogowego, tworzenie komentarzy
- typy danych
- rodzaje operatorów
- instrukcje warunkowe
- tworzenie pętli
- tworzenie podstawowych funkcji
- zasięg zmiennych
- funkcje globalne
- dostępne obiekty w przeglądarce: window, document, history, location, navigator
- zdarzenia (events)

---

# JQuery

- czym jest jQuery
- sposób implementacji do dokumentu HTML
- struktura kodu
- dostęp do elementów strony
- co to jest DOM
- manipulowanie węzłami DOM
- obsługa zdarzeń
- dodawanie animacji na stronie
- widżety jQuery UI

---

# Ajax i JSON

- co to jest Ajax?
- pobieranie treści z serwera
- dynamiczne generowanie treści
- różne formaty danych
- format JSON
- przykład zastosowania wykorzystując jQuery
- przykład zastosowania wykorzystując bibliotekę Axios
